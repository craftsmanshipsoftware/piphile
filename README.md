Open Source DAC for the Raspberry Pi with no compromises!  Burr-Brown audio path from head to toe.

* 2x PCM1794A driving L/R outputs + sub out.
* OPA1612 opamps and OPA1622 output opamp.
* PCM9211 for Inputs, 2 spdif, 1 optical
* PCM1863 for analog input.
* NJU72322 volume IC.
* Optical and coax outputs.
* Steerable clock for synchronization with other piPhiles

### Schematic files

The schematic files are made with KiCad 6.x

### License

Creative Commons BY-NC-SA

See https://creativecommons.org/licenses/by-nc-sa/4.0/